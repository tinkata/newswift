public class Adress {
    private String country;
    private String city;
    private String municipality;
    private int postcode;
    private String street_name;
    private int street_number;
}

   public Adress(String country, String city, String municipality, int postcode, String street_name, int street_number) {
        this.country = country;
        this.city = city;
        this.municipality = municipality;
        this.postcode = postcode;
        this.street_name = street_name;
        this.street_number = street_number;
    }
    public String getCountry() {
        return country;
    }
    public String getCity(){ return  city;}
    public String getMunicipality(){ return  municipality;}
    public int getPostcode(){ return  postcode;}
    public String getStreet_name(){ return  street_name;}
    public int getStreet_number(){ return  street_number;}




}

//<country>;<city>;<municipality>;<postcode>;<street_name>;<street_number>