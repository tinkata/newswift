package lecture09.task03;

public class Ma3x {
    double pricePerHouer;

    Ma3x(double pricePerHouer) {
        this.pricePerHouer = pricePerHouer;
    }

    double charge(Customer customer, int hours) {
        return customer.discount * hours*pricePerHouer;
    }
}
