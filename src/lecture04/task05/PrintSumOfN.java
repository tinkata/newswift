package lecture04.task05;

import java.util.Scanner;

public class PrintSumOfN {
    public static void main(String[] args) {
        System.out.print("num n= ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            int num = scanner.nextInt();
                System.out.printf(" %d ", num);
            sum += num;
        }
        System.out.printf("sum = %d%n",sum);
    }
}
//Напишете програма lecture04.task05.PrintSumOfN,
// която прочита едно число N и после N на брой числа на един ред, разделени с интервал.
// Програмата да отпечатва сбора на въведените числа след приключване на въвеждането.