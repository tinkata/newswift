package lecture04.task06;

import java.util.Scanner;

public class ReadNNumbersOnASingleLine {
    public static void main(String[] args) {
        System.out.print("num n=");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            int num = scanner.nextInt();
            System.out.printf(" %d%n", num);
        }
    }
}
//Напишете програма lecture04.task06.ReadNNumbersOnASingleLine,
// която прочита едно число N и после N на брой числа на един ред
// , разделени с интервал. Програмата да ги отпечата, всяко на нов ред.