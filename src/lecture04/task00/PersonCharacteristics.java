package lecture04.task00;

import java.util.Scanner;

public class PersonCharacteristics {
    public static void main(String[] args) {
        System.out.print("Num. person ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 1; i <=n ; i++) {

        System.out.print("firstName ");
            String firstName = scanner.nextLine();


        System.out.print("lastName ");
        String lastName = scanner.nextLine();//kak da proverqvame za вярността на въведените от клавиатурата данни и при грешка поискайте повторно въвеждане.

        System.out.print("age ");

                int age = scanner.nextInt();
        System.out.print("weight ");
        double weight = scanner.nextDouble();

        System.out.print("height ");
        double height = scanner.nextDouble();

        System.out.print("occupation ");
        String occupation = scanner.nextLine();

            System.out.println();

        if(age<18){
            System.out.printf("%s %s is %d years old. His weight is %.2f kg and he is %.2f cm tall. He is a %s.%s %s is under-aged. ", firstName, lastName, age, weight, height, occupation,firstName, lastName);
        }
        else {
            System.out.printf("%s %s is %d years old. His weight is %.2f kg and he is %.2f cm tall. He is a %s", firstName, lastName, age, weight, height, occupation);
        }
    }
}}

//Добавете проверка дали лицето е непълнолетно. Ако е, добавете следното изречение:
//<first_name> <last_name> is under-aged.
//към края на вече изписаното
//<first_name> <last_name> is <age> years old. His weight is <weight> kg and he is <height> cm tall. He is a <occupation>.
//Проверявайте вярността на въведените от клавиатурата данни и при грешка поискайте повторно въвеждане.
//Променете програмата така, че отначало да се въвежда число N и после характеристики за N на брой отделни индивиди.