package lecture04.task02;

import java.util.Scanner;

public class PrintNonDivisors {

    public static void main(String[] args) {
        System.out.print("n= ");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        for (int i = 1; i <= number; i++) {

            if ((i % 3 != 0) && (i % 7 != 0)) {
                System.out.printf(" %d ",i);

            }

        }
    }
}
