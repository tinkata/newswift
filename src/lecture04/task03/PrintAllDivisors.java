package lecture04.task03;

import java.util.Scanner;

public class PrintAllDivisors {
    public static void main(String[] args) {
        System.out.print("n= ");
        Scanner scanner = new Scanner(System.in);
        int division = scanner.nextInt();

        for (int i = 1; i <= division; i++) {

            if (division % i == 0) {
                System.out.printf("delitel %d%n",i);
            }
        }
    }
}