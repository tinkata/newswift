package lecture04.task07;

import java.util.Scanner;

public class ReadNNumbersOnNewLines {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("number n: ");
        int n = scanner.nextInt();
        String exitNumber = "";
        int number = 0;
        for (int i = 1; i <= n; i++) {
            number = scanner.nextInt();
            exitNumber = exitNumber + number + " ";
        }

        System.out.printf(" %s ", exitNumber);


    }
}


//Напишете програма lecture04.task07.ReadNNumbersOnNewLines,
// която да прочита от първия ред на стандартния вход едно число,
// след което на следващите толкова на брой реда да има по едно число.
// Програмата да ги отпечатва на един ред, разделени с интервал, след приключване на въвеждането.