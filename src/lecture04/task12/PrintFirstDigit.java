package lecture04.task12;

import java.util.Scanner;

public class PrintFirstDigit {
    public static void main(String[] args) {
        System.out.print("num n ");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        while (number >= 10) {
            number/= 10;      // i tuk nerazbiram zashto stava taka kakto v prednata zadacha s obryshtaneto???
        }

        System.out.print(number);

    }
}
