package lecture04.task11;

import java.util.Scanner;

public class PrintMirrorNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        while (n > 0) {

            int lastDigit = n % 10;  // nerazbiam zashto kato delim na 10 se obryshat chislata ne go proumqvam???
            n /= 10;

            System.out.print(lastDigit);
        }

        System.out.println();
    }

}

