package lecture13.task01

class DayOfWeekTest {

    void isWorkDay() {

        //GIVEN
        DayOfWeek day = DayOfWeek.MONDAY;

        //WHEN
        boolean answer = day.isWorkDay();

        //THEN
        Assertions.assertTrue(answer);
    }

}
