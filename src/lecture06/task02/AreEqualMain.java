package lecture06.task02;

public class AreEqualMain {
    public static void main(String[] args) {


        AreEqual areEqual = new AreEqual(3, 3);
        boolean res1 = areEqual.result1(-2, -2);
        boolean res2 = areEqual.result2(9, 6);


        System.out.println();
        System.out.printf("res 1 %s ", res1);
        System.out.println();

        System.out.printf("res 2 %s ", res2);

    }
}
