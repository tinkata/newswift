package lecture03.task00;
import java.util.Scanner;
public class PersonCharacteristics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("firstName ");
        String firstName = scanner.nextLine();
        System.out.print("lastName ");
        String lastName = scanner.nextLine();
        System.out.print("age ");
        int age = scanner.nextInt();
        System.out.print("weight ");
        double weight = scanner.nextDouble();
        System.out.print("height ");
        double height = scanner.nextDouble();
        System.out.print("occupation ");
        String occupation = scanner.nextLine(); // ne mi  dava da vaveda occupation !!!!
        System.out.println();
        System.out.printf("%s %s is %d years old. His weight is %.2f kg and he is %.2f cm tall. He is a %s", firstName, lastName, age, weight, height, occupation);
    }


}
