package lecture03.task05;

import java.util.Scanner;

public class PrintMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("vyvedete chislo ot 0 - 12 ");
        int x = scanner.nextInt();
        if (x <= 0 || x > 12) {
            System.out.println("ERROR");
        }
        if (x == 1) {
            System.out.println("januari");
        }
        if (x == 2) {
            System.out.println("fevruari");
        }
        if (x == 3) {
            System.out.println("mart");
        }
        if (x == 4) {
            System.out.println("april");
        }
        if (x == 5) {
            System.out.println("mai");
        }
        if (x == 6) {
            System.out.println("juni");
        }
        if (x == 7) {
            System.out.println("juli");
        }
        if (x == 8) {
            System.out.println("avgust");
        }
        if (x == 9) {
            System.out.println("septemvri");
        }
        if (x == 10) {
            System.out.println("octomvri");
        }
        if (x == 11) {
            System.out.println("noemvri");
        }
        if (x == 12) {
            System.out.println("decemvri");

        }


    }
}

