package lecture03.task06.a;

import java.util.Scanner;

public class SwapVariables {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("vavedete chislo a = ");
        int a = scanner.nextInt();
        System.out.print("vavedete chislo b = ");
        int b = scanner.nextInt();
        int c = a;
        int d = b;
        b = c;
        a = d;

        System.out.println(a + " " + b);


    }
}
