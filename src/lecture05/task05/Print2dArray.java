package lecture05.task05;

import java.util.Arrays;
import java.util.Scanner;

public class Print2dArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i = 0;
        int j = 0;
        int[][] masiv = new int[n][n];
        // System.out.print(" masiv[" + n + "][" + n + "] ");
        for (i = 0; i < masiv.length; i++) {
            for (j = 0; j < masiv[0].length; j++) {
                System.out.printf("masiv[%d,%d] = ", i, j);
                masiv[i][j] = scanner.nextInt();
            }
        }

        System.out.println(Arrays.deepToString(masiv)); // kak da iskarame rezultatite v konzolata samo kato cisla a ne kato string  ?
    }
}
//Напишете програма lecture05.task05.Print2dArray,
// която да чете от стандартния вход едно число N и да отпечатва матрица NxN с числата от 1 до N*N,
// следвайки формата: