package lecture05.task06;

import java.util.Scanner;

public class Print2dArrayUpAndDownWalk {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] masiv = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int numI = scanner.nextInt();
                int numJ = scanner.nextInt();
                System.out.printf("masiv[%d][%d]", i, j);
            }
        }
    }
}
