package lecture05.task04;

import java.util.Scanner;

public class StringsCharsOnNewLines {
    public static void main(String[] args) {


        String name = "Hallo";
        System.out.println(name.charAt(0));
        System.out.println(name.charAt(1));
        System.out.println(name.charAt(2));
        System.out.println(name.charAt(3));
        System.out.println(name.charAt(4));


    }
}


//Напишете програма lecture05.task04.StringsCharsOnNewLines,
// която да чете от стандартния вход един символен низ и да го отпечата на екрана,
// като всяка буква е на нов ред.