package lecture05.task03;
import java.util.Scanner;
import java.util.Arrays;
public class ReadArray {
    public static void main(String[] args) {
Scanner scanner  = new Scanner(System.in);
int n = scanner.nextInt();
int[] masiv = new int[n];
        for (int i = 0; i < n; i++) {
            int num = scanner.nextInt();
            masiv[i]=num;
        }
        System.out.print("masiv["+n+"]");
        System.out.print(Arrays.toString(masiv));

    }
}
//Напишете програма lecture05.task03.ReadArray,
// която да създава масив от тип int и да го инициализира със стойности,
// въведени от стандартния вход.
// На първия ред на стандартния вход ще бъде въведено число N, което ще указва броя елементи, които ще бъдат въведени след това.
// На втория ред стандартния вход ще бъдат въведени N на брой числа, разделени с интервал.
// Нека създаденият масив да има големина точно N.
// Отпечатайте въведените числа, разделени със запетая, на стандартния изход.
//Можете ли след последното число да не слагате запетая?